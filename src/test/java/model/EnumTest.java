package model;

import cz.ill_j.hibernate.model.GenderEnum;
import org.junit.Assert;
import org.junit.Test;

public class EnumTest {

    @Test
    public void checkEnumValue() {
        // dialogue
        System.out.println(EnumTest.class);
        System.out.println("Checking GenderEnum values.");
        System.out.println(GenderEnum.MAN.toString() + GenderEnum.MAN.getValue());
        System.out.println(GenderEnum.WOMAN.toString() + GenderEnum.WOMAN.getValue());
        // testing
        Assert.assertEquals(1, GenderEnum.MAN.getValue());
        Assert.assertEquals(2, GenderEnum.WOMAN.getValue());
    }
}
