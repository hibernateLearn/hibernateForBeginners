package cz.ill_j.hibernate.util;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * work with Date object
 */
public class DateUtils {

    private static final String DATE_PATTERN = "yyyy-MM-dd";

    /**
     * convert String to Date object
     * @param dateString
     * @return formated date according to pattern
     */
    public static Date getFormatedDate(String dateString) {
        Date date;
        try {
            date = getSimpleDateFormat().parse(dateString);
        } catch (ParseException e) {
            System.out.println("Error while Date parsing.");
            throw new IllegalStateException("Field date can't be NULL.", e);
        }
        return date;
    }

    private static SimpleDateFormat getSimpleDateFormat() {
        return new SimpleDateFormat(DATE_PATTERN);
    }
}
