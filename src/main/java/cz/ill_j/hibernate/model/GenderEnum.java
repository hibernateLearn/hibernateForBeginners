package cz.ill_j.hibernate.model;

public enum GenderEnum {
    MAN(1),
    WOMAN(2);

    private int gen;

    GenderEnum(int gen) {
        this.gen = gen;
    }

    public int getValue() {
        return gen;
    }
}
